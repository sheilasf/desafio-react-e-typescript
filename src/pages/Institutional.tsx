import React from "react";
import { Header } from "../components/Header/Header";
import { MainPage } from "../components/Main/Main";
import { Newsletter } from "../components/Newsletter/Newsletter";
import { Footer } from "../components/Footer/Footer";
import { PageButtons } from "../components/PageButtons/PageButtons";

import "../common/global.css";
import "../common/reset.css";

function Institutional() {
  return (
    <>
      <Header />
      <MainPage />
      <PageButtons />
      <Newsletter />
      <Footer />
    </>
  );
}

export { Institutional };
