import React, { useState } from "react";
import styles from "./Footer.module.css";

import FacebookIcon from "./images/facebook-icon.svg";
import InstagramIcon from "./images/instagram-icon.svg";
import TwitterIcon from "./images/twitter-icon.svg";
import YoutubeIcon from "./images/youtube-icon.svg";
import LinkedinIcon from "./images/linkedin-icon.svg";
import MasterCardIcon from "./images/master-icon.svg";
import VisaIcon from "./images/visa-icon.svg";
import DinersIcon from "./images/diners-icon.svg";
import EloIcon from "./images/elo-icon.svg";
import HiperIcon from "./images/hiper-icon.svg";
import PaypalIcon from "./images/paypal-icon.svg";
import BoletoIcon from "./images/boleto-icon.svg";
import VtexPCI from "./images/vtex-pci-200.svg";
import VtexLogo from "./images/vtex-logo.svg";
import M3FooterLogo from "./images/m3-footer-logo.svg";

const Footer = () => {
  const [Institutional, setInstitutional] = useState(false);
  const [Query, setQuery] = useState(false);
  const [Questions, setQuestions] = useState(false);
  const openInstitutional = () => {
    Institutional ? setInstitutional(false) : setInstitutional(true);
  };
  const openQuery = () => {
    Query ? setQuery(false) : setQuery(true);
  };
  const openQuestions = () => {
    Questions ? setQuestions(false) : setQuestions(true);
  };

  return (
    <>
      <section className={styles["footer-desktop"]}>
        <div className={styles["footer-wrapper"]}>
          <div className={styles["footer-content"]}>
            <h3>
              <a href="/" target="_blank">
                Institucional
              </a>
            </h3>
            <ul className="footer-list">
              <li>
                <a href="/">Quem Somos</a>
              </li>
              <li>
                <a href="/">Política de Privacidade</a>
              </li>
              <li className={styles["footer-list-link"]}>
                <a href="/">Segurança</a>
              </li>
              <li>
                <a className={styles["footer-list-link-line"]} href="/">
                  Seja um Revendedor
                </a>
              </li>
            </ul>
          </div>

          <div className={styles["footer-content-col-02"]}>
            <h3>
              <a href="/" target="_blank">
                Dúvidas
              </a>
            </h3>
            <ul className="footer-list">
              <li>
                <a href="/">Entrega</a>
              </li>
              <li className={styles["footer-list-link"]}>
                <a href="/">Pagamento</a>
              </li>
              <li>
                <a href="/">Troca e devoluções</a>
              </li>
              <li>
                <a className={styles["footer-list-link-line"]} href="/">
                  Dúvidas Frequentes
                </a>
              </li>
            </ul>
          </div>

          <div className={styles["footer-content-col-3"]}>
            <h3>
              <a href="/" target="_blank">
                Fale Conosco
              </a>
            </h3>
            <ul className="footer-list">
              <li className={styles["footer-list-link-subtitle"]}>
                Atendimento ao Consumidor
              </li>
              <li className={styles["footer-list-link-number"]}>
                (11) 4159 9504
              </li>
              <li className={styles["footer-list-link-subtitle"]}>
                Atendimento Online
              </li>
              <li className={styles["footer-list-link-number"]}>
                (11) 99433-8825
              </li>
            </ul>
          </div>

          <div className={styles["footer-wrapper-column-icons"]}>
            <div className={styles["footer-wrapper-icons"]}>
              <a href="/" target="_blank">
                <img
                  className={styles["footer-icons-facebook"]}
                  src={FacebookIcon}
                  alt="Icone do Facebook"
                />
              </a>
              <a href="/" target="_blank">
                <img
                  className={styles["footer-icons-instagram"]}
                  src={InstagramIcon}
                  alt="Icone do Instagram"
                />
              </a>
              <a href="/" target="_blank">
                <img
                  className={styles["footer-icons-twitter"]}
                  src={TwitterIcon}
                  alt="Icone do Twitter"
                />
              </a>
              <a href="/" target="_blank">
                <img
                  className={styles["footer-icons-youtube"]}
                  src={YoutubeIcon}
                  alt="Icone do Youtube"
                />
              </a>
              <a href="/" target="_blank">
                <img
                  className={styles["footer-icons-linkedin"]}
                  src={LinkedinIcon}
                  alt="Icone do Linkedin"
                />
              </a>
            </div>
            <a href="/" target="_blank">
              www.loremipsum.com
            </a>
          </div>
        </div>
      </section>

      <section className="bottom-footer">
        <div className={styles["footer-bottom"]}>
          <p className={styles["footer-bottom-text"]}>
            Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing
            <br />
            Elit, Sed Do Eiusmod Tempor
          </p>
          <div className={styles["footer-icons-wrapper"]}>
            <img
              className={styles["footer-icons-payment"]}
              src={MasterCardIcon}
              alt="Logo Mastercard"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={VisaIcon}
              alt="Logo Visa"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={DinersIcon}
              alt="Logo American Express"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={EloIcon}
              alt="Logo Elo"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={HiperIcon}
              alt="Logo Hipercard"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={PaypalIcon}
              alt="Logo Paypal"
            />
            <img className={styles["footer-icons-payment"]} src={BoletoIcon} />
            <div className={styles["footer-icons-vtex-pci"]} />
            <img
              className="footer-icons-vtex-pci"
              src={VtexPCI}
              alt="Logo Vtex Pci"
            />
          </div>
          <div className={styles["footer-icons-wrapper"]}>
            <a
              className={styles["footer-icons-wrapper-vtex"]}
              href="https://vtex.com/"
              target="_blank"
              rel="noreferrer"
            >
              <p className={styles["footer-icons-vtex-text"]}>Powered by</p>
              <img
                className={styles["footer-icons-vtex-img"]}
                src={VtexLogo}
                alt="Logo Vtex"
              />
            </a>
            <a
              className={styles["footer-icons-wrapper-logo"]}
              href="https://m3ecommerce.com/"
              target="_blank"
              rel="noreferrer"
            >
              <p className={styles["footer-icons-logo-text"]}>Developed by</p>
              <img
                className={styles["footer-icons-logo-img"]}
                src={M3FooterLogo}
                alt="Logo M3"
              />
            </a>
          </div>
        </div>
      </section>
      {/*Footer Mobile*/}

      <section className={styles["footer-mobile"]}>
        <div className={styles["footer-button-mobile-institutional"]}>
          <h5 className={styles["footer-button-title-mobile"]}>
            Institucional
          </h5>
          <button
            onClick={openInstitutional}
            className={styles["footer-button-icon-mobile"]}
          >
            <div
              className={
                styles[
                  Institutional
                    ? "footer-button-icon-mobile-no-add"
                    : "footer-button-icon-mobile-add"
                ]
              }
            ></div>
            <div className={styles["footer-button-icon-mobile-remove"]}></div>
          </button>
        </div>
        <div
          className={
            styles[
              Institutional ? "footer-wrapper-infos" : "footer-wrapper-no-infos"
            ]
          }
        >
          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span className={styles["footer-wrapper-info-subtitle"]}>
              Quem Somos
            </span>
          </a>

          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>Política de Privacidade</span>
          </a>

          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>Segurança</span>
          </a>

          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span className={styles["footer-wrapper-info-line"]}>
              Seja um Revendedor
            </span>
          </a>
        </div>
        <div className={styles["footer-button-mobile-institutional"]}>
          <h5 className={styles["footer-button-title-mobile"]}>Dúvidas</h5>
          <button
            onClick={openQuery}
            className={styles["footer-button-icon-mobile"]}
          >
            <div
              className={
                styles[
                  Query
                    ? "footer-button-icon-mobile-no-add"
                    : "footer-button-icon-mobile-add"
                ]
              }
            ></div>
            <div className={styles["footer-button-icon-mobile-remove"]}></div>
          </button>
        </div>
        <div
          className={
            styles[Query ? "footer-wrapper-infos" : "footer-wrapper-no-infos"]
          }
        >
          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>Entrega</span>
          </a>
          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>Pagamento</span>
          </a>
          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>Troca e Devoluções</span>
          </a>
          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>Dúvidas Frequentes</span>
          </a>
        </div>
        <div className={styles["footer-button-mobile-institutional"]}>
          <h5 className={styles["footer-button-title-mobile"]}>Fale Conosco</h5>
          <button
            onClick={openQuestions}
            className={styles["footer-button-icon-mobile"]}
          >
            <div
              className={
                styles[
                  Questions
                    ? "footer-button-icon-mobile-no-add"
                    : "footer-button-icon-mobile-add"
                ]
              }
            ></div>
            <div className={styles["footer-button-icon-mobile-remove"]}></div>
          </button>
        </div>
        <div
          className={
            styles[
              Questions ? "footer-wrapper-infos" : "footer-wrapper-no-infos"
            ]
          }
        >
          <span className={styles["footer-wrapper-info-bold"]}>
            {" "}
            Atendimento ao Consumidor{" "}
          </span>

          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span>(11) 4159 9504</span>
          </a>

          <span className={styles["footer-wrapper-info-bold"]}>
            Atendimento Online
          </span>

          <a
            className={styles["footer-wrapper-info"]}
            href="/"
            target="_blank"
            rel="noreferrer"
          >
            <span> (11) 99433-8825 </span>
          </a>
        </div>
        <div className={styles["footer-wrapper-icons"]}>
          <a href="/" target="_blank" rel="noreferrer">
            <img
              className={styles["footer-wrapper-icons-click-facebook"]}
              src={FacebookIcon}
              alt="Facebook icon"
            />
          </a>
          <a href="/" target="_blank" rel="noreferrer">
            <img
              className={styles["footer-wrapper-icons-click-instagram"]}
              src={InstagramIcon}
              alt="Instagram icon"
            />
          </a>
          <a href="/" target="_blank" rel="noreferrer">
            <img
              className={styles["footer-wrapper-icons-click-twitter"]}
              src={TwitterIcon}
              alt="Twitter icon"
            />
          </a>
          <a href="/" target="_blank" rel="noreferrer">
            <img
              className={styles["footer-wrapper-icons-click-youtube"]}
              src={YoutubeIcon}
              alt="Youtube icon"
            />
          </a>
          <a href="/" target="_blank" rel="noreferrer">
            <img
              className={styles["footer-wrapper-icons-click-linkedin"]}
              src={LinkedinIcon}
              alt="Linkedin icon"
            />
          </a>
        </div>
        <div className={styles["footer-mobile-icons"]}>
          <div className={styles["footer-icons-wrapper"]}>
            <img
              className={styles["footer-icons-payment"]}
              src={MasterCardIcon}
              alt="Logo Mastercard"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={VisaIcon}
              alt="Logo Visa"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={DinersIcon}
              alt="Logo American Express"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={EloIcon}
              alt="Logo Elo"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={HiperIcon}
              alt="Logo Hipercard"
            />
            <img
              className={styles["footer-icons-payment"]}
              src={PaypalIcon}
              alt="Logo Paypal"
            />
            <img
              className={styles["footer-icons-payment-boleto"]}
              src={BoletoIcon}
              alt="Logo Boleto"
            />

            <img
              className={styles["footer-icons-vtex-pci"]}
              src={VtexPCI}
              alt="Logo Vtex Pci"
            />
          </div>
          <div className={styles["footer-botton-mobile"]}>
            <p className={styles["footer-text-mobile"]}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. .
            </p>
          </div>
          <div className={styles["footer-icons-wrapper-mobile"]}>
            <a
              className={styles["footer-icons-wrapper-vtex"]}
              href="https://vtex.com/"
              target="_blank"
              rel="noreferrer"
            >
              <p className={styles["footer-icons-vtex-text"]}>Powered by</p>
              <img
                className="footer-icons-vtex-img"
                src={VtexLogo}
                alt="Logo Vtex"
              />
            </a>
            <a
              className={styles["footer-icons-wrapper-logo"]}
              href="https://m3ecommerce.com/"
              target="_blank"
              rel="noreferrer"
            >
              <p className={styles["footer-icons-logo-text"]}>Developed by</p>
              <img
                className={styles["footer-icons-logo-img"]}
                src={M3FooterLogo}
                alt="Logo M3"
              />
            </a>
          </div>
        </div>
      </section>
    </>
  );
};

export { Footer };
