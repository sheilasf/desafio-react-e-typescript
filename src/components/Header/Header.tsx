import React, { useState } from "react";
import styles from "./Header.module.css";
import Logo from "../../assets/images/logo.svg";
import CartIcon from "../../assets/images/cart-icon.svg";
import { Search } from "../Search/Search";
import { Breadcrumb } from "../Breadcrumb/Breadcrumb";
import { MenuHeader } from "../Menu/MenuHeader";

const Header = () => {
  return (
    <>
      <header className={styles["header-desktop"]}>
        <div className={styles["header-wrapper-desktop"]}>
          <a href="/">
            <img
              className={styles["header-logo-desktop"]}
              src={Logo}
              alt="Logo M3 Academy"
            />
          </a>
          <Search />
          <div className={styles["header-buttons-desktop"]}>
            <a
              className={styles["header-login-desktop"]}
              href="/"
              target="_blank"
            >
              ENTRAR
            </a>
            <a href="/">
              <img
                className={styles["cart-icon-desktop"]}
                src={CartIcon}
                alt="Ícone do Carrinho"
              />
            </a>
          </div>
        </div>
        <MenuHeader />
        <Breadcrumb />
      </header>

      <header className={styles["header-mobile"]}>
        <div className={styles["header-mobile-content"]}>
          <div
            className={`${styles["header-mobile-area"]} ${styles["area-side"]}`}
          >
            <MenuHeader />
          </div>

          <div
            className={`${styles["header-mobile-area"]} ${styles["area-center"]}`}
          >
            <div>
              <a href="/">
                {" "}
                <img src={Logo} alt="Logo M3" />{" "}
              </a>
            </div>
          </div>

          <div
            className={`${styles["header-mobile-area"]} ${styles["area-right"]}`}
          >
            <div>
              <a href="/">
                <img
                  className={styles["header-mobile-icons-cart"]}
                  src={CartIcon}
                  alt="Ícone do Carrinho"
                />
              </a>
            </div>
          </div>
        </div>
        <div
          className={`${styles["header-mobile-area"]} ${styles["area-bottom"]}`}
        >
          <Search />
        </div>
      </header>
    </>
  );
};

export { Header };
