import React, { useState } from "react";
import styles from "./Main.module.css";
import { ContactForm } from "../ContactForm/ContactForm";

const MainPage = () => {
  const [About, setAbout] = useState(true);
  const [Payment, setPayment] = useState(false);
  const [Delivery, setDelivery] = useState(false);
  const [Devolution, setDevolution] = useState(false);
  const [showInformationsSecurity, setShowInformationsSecurity] =
    useState(false);
  const [showInformationsContact, setShowInformationsContact] = useState(false);

  const toggleAbout = () => {
    setAbout(true);
    setShowInformationsContact(false);
    setDelivery(false);
    setDevolution(false);
    setPayment(false);
    setShowInformationsSecurity(false);
  };

  const togglePayment = () => {
    setPayment(true);
    setAbout(false);
    setShowInformationsContact(false);
    setDelivery(false);
    setDevolution(false);
    setShowInformationsSecurity(false);
  };

  const toggleDelivery = () => {
    setDelivery(true);
    setAbout(false);
    setShowInformationsContact(false);
    setDevolution(false);
    setPayment(false);
    setShowInformationsSecurity(false);
  };

  const toggleDevolution = () => {
    setDevolution(true);
    setAbout(false);
    setShowInformationsContact(false);
    setDelivery(false);
    setPayment(false);
    setShowInformationsSecurity(false);
  };

  const toggleSecurity = () => {
    setShowInformationsSecurity(true);
    setAbout(false);
    setShowInformationsContact(false);
    setDelivery(false);
    setDevolution(false);
    setPayment(false);
  };

  const toggleContact = () => {
    setShowInformationsContact(true);
    setAbout(false);
    setDelivery(false);
    setDevolution(false);
    setPayment(false);
    setShowInformationsSecurity(false);
  };

  return (
    <>
      <div className={styles["main-page"]}>
        <div className={styles["main-page-title"]}>
          <h1>INSTITUCIONAL</h1>
        </div>
        <div className={styles["main-page-wrapper"]}>
          <div className={styles["main-page-title-wrapper"]}>
            <button
              onClick={toggleAbout}
              className={
                styles[
                  About ? "main-menu-title-active" : "main-menu-title-style"
                ]
              }
            >
              Sobre
            </button>
            <button
              onClick={togglePayment}
              className={
                styles[
                  Payment ? "main-menu-title-active" : "main-menu-title-style"
                ]
              }
            >
              Forma de Pagamento
            </button>
            <button
              onClick={toggleDelivery}
              className={
                styles[
                  Delivery ? "main-menu-title-active" : "main-menu-title-style"
                ]
              }
            >
              Entrega
            </button>
            <button
              onClick={toggleDevolution}
              className={
                styles[
                  Devolution
                    ? "main-menu-title-active"
                    : "main-menu-title-style"
                ]
              }
            >
              Troca e Devolução
            </button>
            <button
              onClick={toggleSecurity}
              className={
                styles[
                  showInformationsSecurity
                    ? "main-menu-title-active"
                    : "main-menu-title-style"
                ]
              }
            >
              Segurança e Privacidade
            </button>
            <button
              className={
                styles[
                  showInformationsContact
                    ? "main-menu-title-active"
                    : "main-menu-title-style"
                ]
              }
              onClick={toggleContact}
            >
              Contato
            </button>
          </div>
          <div className={styles["main-menu-details-wrapper"]}>
            <div className={styles[About ? "main-menu-details" : "closed"]}>
              <h2 className={styles["main-menu-details-title"]}>Sobre</h2>
              <p className={styles["main-menu-details-text"]}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <br />
              <p className={styles["main-menu-details-text"]}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem.
              </p>
              <br />
              <p
                className={`${styles["main-menu-details-text"]} ${styles["main-menu-details-mobile"]}`}
              >
                Ut enim ad minima veniam, quis nostrum exercitationem ullam
                corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                consequatur? Quis autem vel eum iure reprehenderit qui in ea
                voluptate velit esse quam nihil molestiae consequatur, vel illum
                qui dolorem eum fugiat quo voluptas nulla pariatur?
              </p>
            </div>
            <div className={styles[Payment ? "main-menu-details" : "closed"]}>
              <h2 className={styles["main-menu-details-title"]}>
                Forma de Pagamento
              </h2>
              <p className={styles["main-menu-details-text"]}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <br />
              <p className={styles["main-menu-details-text"]}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem.
              </p>
              <br />
              <p
                className={`${styles["main-menu-details-text"]} ${styles["main-menu-details-mobile"]}`}
              >
                Ut enim ad minima veniam, quis nostrum exercitationem ullam
                corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                consequatur? Quis autem vel eum iure reprehenderit qui in ea
                voluptate velit esse quam nihil molestiae consequatur, vel illum
                qui dolorem eum fugiat quo voluptas nulla pariatur?
              </p>
            </div>
            <div className={styles[Delivery ? "main-menu-details" : "closed"]}>
              <h2 className={styles["main-menu-details-title"]}>Entrega</h2>
              <p className={styles["main-menu-details-text"]}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <br />
              <p className={styles["main-menu-details-text"]}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem.
              </p>
              <br />
              <p
                className={`${styles["main-menu-details-text"]} ${styles["main-menu-details-mobile"]}`}
              >
                Ut enim ad minima veniam, quis nostrum exercitationem ullam
                corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                consequatur? Quis autem vel eum iure reprehenderit qui in ea
                voluptate velit esse quam nihil molestiae consequatur, vel illum
                qui dolorem eum fugiat quo voluptas nulla pariatur?
              </p>
            </div>
            <div
              className={styles[Devolution ? "main-menu-details" : "closed"]}
            >
              <h2 className={styles["main-menu-details-title"]}>
                Troca e Devolução
              </h2>
              <p className={styles["main-menu-details-text"]}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <br />
              <p className={styles["main-menu-details-text"]}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem.
              </p>
              <br />
              <p
                className={`${styles["main-menu-details-text"]} ${styles["main-menu-details-mobile"]}`}
              >
                Ut enim ad minima veniam, quis nostrum exercitationem ullam
                corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                consequatur? Quis autem vel eum iure reprehenderit qui in ea
                voluptate velit esse quam nihil molestiae consequatur, vel illum
                qui dolorem eum fugiat quo voluptas nulla pariatur?
              </p>
            </div>
            <div
              className={
                styles[
                  showInformationsSecurity ? "main-menu-details" : "closed"
                ]
              }
            >
              <h2 className={styles["main-menu-details-title"]}>
                Segurança e Privacidade
              </h2>
              <p className={styles["main-menu-details-text"]}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <br />
              <p className={styles["main-menu-details-text"]}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem.
              </p>
              <br />
              <p
                className={`${styles["main-menu-details-text"]} ${styles["main-menu-details-mobile"]}`}
              >
                Ut enim ad minima veniam, quis nostrum exercitationem ullam
                corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                consequatur? Quis autem vel eum iure reprehenderit qui in ea
                voluptate velit esse quam nihil molestiae consequatur, vel illum
                qui dolorem eum fugiat quo voluptas nulla pariatur?
              </p>
            </div>
            <div
              className={
                styles[
                  showInformationsContact ? "main-menu-details-form" : "closed"
                ]
              }
            >
              <ContactForm />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export { MainPage };
