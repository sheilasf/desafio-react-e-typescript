import React from "react";
import styles from "./Search.module.css";
import SearchIcon from "../../assets/images/search-icon.svg";

const Search = () => {
  return (
    <>
      <div className={styles["wrapper-search"]}>
        <form className={styles["search-form"]}>
          <input
            className={styles["search-input"]}
            type="text"
            name="input"
            placeholder="Buscar..."
          />
        </form>
        <img className={styles["search-icon"]} alt="Search" src={SearchIcon} />
      </div>
    </>
  );
};

export { Search };
