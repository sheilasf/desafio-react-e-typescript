import React from "react";
import styles from "./Newsletter.module.css";

import { Formik, Form, Field, ErrorMessage } from "formik";
import FormSchemaNewsletter from "../../schema/NewsletterSchema";

interface IFormikValues {
  email: String;
}
const initialValues = {
  email: "",
};

const Newsletter: React.FC = () => {
  const handleClick = (values: IFormikValues, { resetForm }: any) => {
    if (values) {
      alert("Newsletter assinada com sucesso!");
      resetForm();
    }
  };

  return (
    <section className={styles["newsletter-wrapper"]}>
      <Formik
        onSubmit={handleClick}
        initialValues={initialValues}
        validationSchema={FormSchemaNewsletter}
      >
        <Form>
          <div className={styles["newsletter"]}>
            <div className={styles["newsletter-wrapper-content"]}>
              <h5 className={styles["newsletter-title"]}>
                assine nossa newsletter
              </h5>
              <Field
                className={styles["newsletter-input"]}
                name="email"
                placeholder="E-mail"
              />
              <ErrorMessage
                component="h5"
                name="email"
                className={styles["error-mesage-inputs"]}
              />
            </div>
            <div className={styles["news-button"]}>
              <button type="submit">Enviar</button>
            </div>
          </div>
        </Form>
      </Formik>
    </section>
  );
};

export { Newsletter };
