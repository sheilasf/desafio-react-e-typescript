import React from "react";
import styles from "./PageButtons.module.css";

import WhatsappLogo from "../../assets/images/whatsapp-icon.svg";
import TopButton from "../../assets/images/top-button.svg";

const scrollTop = () => {
  window.scrollTo({ top: 0, behavior: "smooth" });
};

const PageButtons = () => {
  return (
    <>
      <div className={styles["page-buttons"]}>
        <img
          className={styles["whatsapp-icon"]}
          src={WhatsappLogo}
          alt="Logo do Whatsapp"
        />
        <div className="button-up" onClick={scrollTop}>
          <a className={styles["top-button"]}>
            <img src={TopButton} alt="Botão para voltar ao topo da página." />
          </a>
        </div>
      </div>
    </>
  );
};

export { PageButtons };
