import React, { useState } from "react";
import styles from "./MenuHeader.module.css";
import MenuIcon from "../../assets/images/mobile-menu-icon.svg";
import CloseIcon from "../../assets/images/close-icon.svg";

const MenuHeader = () => {
  const [active, setMode] = useState(false);

  const ToggleMode = () => {
    setMode(!active);
  };

  return (
    <>
      <div className="menu-header-desktop">
        <div className={styles["menu-desktop-wrapper"]}>
          <div className={styles["menu-header-desktop-content"]}>
            <a
              href="/"
              className={styles["menu-desktop-item"]}
              target="_blank"
              rel="noreferrer"
            >
              CURSOS
            </a>
            <a
              href="/"
              className={styles["menu-desktop-item-02"]}
              target="_blank"
              rel="noreferrer"
            >
              SAIBA MAIS
            </a>
          </div>
        </div>
      </div>

      <div className={styles["menu-header-mobile"]}>
        <nav className={styles["menu"]} id="menu">
          <div
            className={styles[active ? "iconActive" : ""]}
            onClick={ToggleMode}
          >
            <button className={styles["icon-button"]}>
              <img
                src={MenuIcon}
                className={styles["icon-menu-header"]}
                alt=" Icone do Menu"
              ></img>
            </button>
          </div>
          <div
            className={styles[active ? "menu-overlay" : ""]}
            onClick={ToggleMode}
          ></div>
          <div className={styles[active ? "menuOpen" : "menuClose"]}>
            <div className={styles["menu-modal"]}>
              <div className={styles["menu-content"]}>
                <div className={styles["menu-user"]}>
                  <a href="/" className={styles["user-link"]}>
                    <span> Entrar</span>
                  </a>
                  <div
                    className={styles[active ? "close-icon" : "close"]}
                    onClick={ToggleMode}
                  >
                    <button
                      className={`${styles["menu-close-icon"]} ${styles["icon-button"]}`}
                    >
                      <img
                        src={CloseIcon}
                        className={styles["icon-menu-close"]}
                        alt=" Icone do Menu"
                      ></img>
                    </button>
                  </div>
                </div>
                <div className={styles["menu-panel"]}>
                  <ul>
                    <li className={styles["menu-item"]}>
                      <a
                        href="/"
                        className={`${styles["menu-handle"]} ${styles["item-1"]}`}
                      >
                        <span> Cursos</span>
                      </a>
                    </li>
                    <li className={styles["menu-item"]}>
                      <a
                        href="/"
                        className={`${styles["menu-handle"]} ${styles["item-2"]}`}
                      >
                        <span> Saiba Mais</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </>
  );
};

export { MenuHeader };
