import React from "react";
import styles from "./Breadcrumb.module.css";

import ArrowIcon from "../../assets/images/arrow-icon.svg";
import HomeIcon from "../../assets/images/home-icon.svg";

const Breadcrumb = () => {
  return (
    <div className={styles["middle-icons"]}>
      <a href="/">
        <img
          className={styles["middle-icons-home"]}
          src={HomeIcon}
          alt="Home icon"
        />
      </a>
      <img src={ArrowIcon} alt="Arrow icon" />
      <a className={styles["middle-icons-title"]} href="/">
        INSTITUCIONAL
      </a>
    </div>
  );
};

export { Breadcrumb };
