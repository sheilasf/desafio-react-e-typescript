import React from "react";
//import InputMask from "react-input-mask";
import { Formik, Form, Field, ErrorMessage } from "formik";
import FormSchema from "../../schema/FormSchema";
import styles from "./ContactForm.module.css";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  birthDate: string;
  phone: string;
  instagram: string;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  birthDate: "",
  phone: "",
  instagram: "",
};

const ContactForm: React.FC = () => {
  const handleFormikSubmit = (values: IFormikValues, { resetForm }: any) => {
    if (values) {
      alert("Formulário enviado com sucesso!");
      resetForm();
    }
  };

  return (
    <div className={styles["form-wrapper"]}>
      <Formik
        onSubmit={handleFormikSubmit}
        initialValues={initialValues}
        validationSchema={FormSchema}
      >
        {({ errors, touched }) => (
          <Form>
            <h2>Preencha o Formulário </h2>
            <div className={styles["form-col"]}>
              <label htmlFor="name">Nome</label>
              <Field
                id="name"
                name="name"
                className={errors.name && touched.name && "invalid"}
                placeholder="Seu nome completo"
              />
              <ErrorMessage
                component="span"
                name="name"
                className={styles["form-invalid-feedback"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="email">E-mail</label>
              <Field
                id="email"
                name="email"
                className={errors.email && touched.email && "invalid"}
                placeholder="Seu e-mail"
              />
              <ErrorMessage
                component="span"
                name="email"
                className={styles["form-invalid-feedback"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="cpf">CPF</label>
              <Field
                id="cpf"
                name="cpf"
                className={errors.cpf && touched.cpf && "invalid"}
                placeholder="000 000 000 00"
              />
              <ErrorMessage
                component="span"
                name="cpf"
                className={styles["form-invalid-feedback"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="birthDate">Data de Nascimento:</label>
              <Field
                id="birthDate"
                name="birthDate"
                className={errors.birthDate && touched.birthDate && "invalid"}
                placeholder="00 . 00 . 0000"
              />
              <ErrorMessage
                component="span"
                name="birthDate"
                className={styles["form-invalid-feedback"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="phone">Telefone:</label>
              <Field
                id="phone"
                name="phone"
                className={errors.phone && touched.phone && "invalid"}
                placeholder="(00) 00000 0000"
              />
              <ErrorMessage
                component="span"
                name="phone"
                className={styles["form-invalid-feedback"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="instagram">Instagram</label>
              <Field
                id="instagram"
                name="instagram"
                className={errors.instagram && touched.instagram && "invalid"}
                placeholder="@seuuser"
              />
              <ErrorMessage
                component="span"
                name="instagram"
                className={styles["form-invalid-feedback"]}
              />
            </div>

            <div className={styles["form-terms"]}>
              <span className={styles["form-term-asterisk"]}>*</span>
              <a className={styles["form-term-accept"]} href="/">
                {" "}
                Declaro que li e aceito
              </a>

              <div className={styles["checkbox-custom"]}>
                <input
                  className={styles["input-checkbox"]}
                  type="checkbox"
                  value="None"
                  id="form-terms-label"
                  name="check"
                />
                <label
                  className={styles["input-label"]}
                  htmlFor="form-terms-label"
                ></label>
              </div>
            </div>
            <button type="submit">Cadastre-se</button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export { ContactForm };
