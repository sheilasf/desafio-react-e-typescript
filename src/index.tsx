import React from "react";
import ReactDOM from "react-dom/client";
import reportWebVitals from "./reportWebVitals";
import { Institutional } from "./pages/Institutional";

reportWebVitals();

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(<Institutional />);
