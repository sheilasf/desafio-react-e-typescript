import * as Yup from "yup";

export default Yup.object().shape({
  name: Yup.string()
    .matches(/(?=.{3})/, "Nome muito curto")
    .required("Campo obrigatório"),
  email: Yup.string().required("Campo obrigatório").email("E-mail inválido"),
  cpf: Yup.string()
    .matches(
      /([0-9]{3} [0-9]{3} [0-9]{3} [0-9]{2})/,
      "Inserir CPF no formato 000 000 000 00"
    )
    .required("Campo obrigatório"),
  birthDate: Yup.string()
    .matches(
      /(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})/,
      "Formato de data inválido"
    )
    .required("Campo obrigatório"),
  phone: Yup.string()
    .matches(
      /(\([0-9]{2}\) 9[0-9]{4} [0-9]{4})/,
      "Formato de número de telefone inválido"
    )
    .required("Campo obrigatório"),
  instagram: Yup.string()
    .matches(/@/, "Formato de user inválido")
    .required("Campo obrigatório"),
});
